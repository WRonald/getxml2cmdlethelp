﻿using System.Management.Automation;  // Windows PowerShell assembly.
using System;
using System.Collections.Generic;
using System.IO;
using Passwords;
using System.Linq;
using Sample;


namespace CmdletTest.GetValidationQuantity

{
    /// <summary>
    /// <para type="synopsis">Gets all valid passports from given file.</para>
    /// <para type="description">The 'Get-ValidationQuantity' cmdlet gets all valid passwords from the file of the given path. All passwords are validated by the Validation function, with the given requirements.</para>
    /// <para type="description">The 'Get-ValidationQuantity' cdmlet decides whether a password is valid by looking at the previously specified amount of a previously set letter.</para>
    /// <para type="description">The password is seen as valid when the required specification are met.</para>
    /// <para type="syntax">Get-ValidationPosition</para>
    /// <para type="inputType">System.String</para>
    /// <para type="inputTypeDescription">You can Input the path of the given txt file, with the passwords in it.</para>
    /// <para type="inputTypeDescription">The given Input will be seen as a String and used to locate the file you are referring to.</para>
    /// <para type="returnValue">System.String</para>
    /// <para type="returnValueDescription">The cmdlet will show you how many passwords are valid with the given requirements, inside the given textfile.</para>
    /// <para type="alertTitle">Style Requirements for the passwords inside the txtfile</para>
    /// <para type="alert">(number)-(number) (letter): (password)</para>
    /// <para type="alert">Note that the passwords withing the given txtfile have to be structured exactly like shown above (without the parenthesis)</para>
    /// <para type="linkTitle">Get-Test</para>
    /// <para type="linkTitle">Google:</para>
    /// <para type="link">https://www.google.de</para>
    /// <para type="linkTitle">Get-ValidationPosition</para>
    /// <para type="linkTitle">Send-Greeting</para>
    /// <para type="linkTitle">Get-Gizmo</para>
    /// </summary>
    [Cmdlet(VerbsCommon.Get, "ValidationQuantity")]
    public class GetValidationQuantity : Cmdlet
    {
        void PasswordValidation()
        {
            Console.WriteLine(@"C:\DEV\C#Einführung\Code\TestCmdlet\TestCmdlet2\txts\passwords.txt");
            Console.WriteLine("Enter the path to your txt file: ");
  #pragma warning disable CS8604 // Possible null reference argument.
            string passwords = File.ReadAllText(Console.ReadLine());
  #pragma warning restore CS8604 // Possible null reference argument.
            var passwordList = new List<Password>();
            var lines = passwords.Split("\n");
            for (int i = 0; i < lines.Length; i++)
            {
                var keywords = lines[i].Split(":");
                var indicators = keywords[0].Split(" ");
                var numbers = indicators[0].Split("-");

                var password = new Password(keywords[1].Trim(), indicators[1], numbers[0], numbers[1]);
                passwordList.Add(password);
            }
            Console.WriteLine("Number of Valid passwords (condition Quantity): " + passwordList.Count(p => p.IsValidQuantity()));
        }
        protected override void ProcessRecord()
        {
            PasswordValidation();
        }
    }
}
