﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using Gizmo;

namespace Sample
{
    /// <summary>
    /// <para type="synopsis">Asks the user a flavor. (Currently only Vanilla,Chocolate and Marshmallow are available.</para>
    /// <para type="description">Asks the user a flavor, which the Cmdlet will store for later use. Currently this has no use and is only a test.</para>
    /// <para type="syntax">Get-Gizmo</para>
    /// <para type="inputType">System.String</para>
    /// <para type="inputTypeDescription">Takes the input and uses it as a string</para>
    /// <para type="linkTitle">Google</para>
    /// <para type="link">www.Google.com</para>
    /// <para type="linkTitle">Get-ValidationQuantity</para>
    /// <para type="linkTitle">Get-ValidationPosition</para>
    /// <para type="linkTitle">Send-Greeting</para>
    /// </summary>
    [Cmdlet(VerbsCommon.Get, "Test")]
    /// <summary>
    /// <para>Testender Test</para>
    /// <para>those events will be skipped</para>
    /// this will also be skipped
    /// </summary>
    public class GetTest : Cmdlet
    {
        void Test()
        {

            Console.WriteLine("\nTest\n");
        }
        protected override void ProcessRecord()
        {
            Test();
        }
    }
}