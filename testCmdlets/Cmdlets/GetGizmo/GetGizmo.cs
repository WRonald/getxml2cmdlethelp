﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using Template;

namespace Gizmo
{
    /// <summary>
    /// <para type="synopsis">This is the cmdlet synopsis.</para>
    /// <para type="description">This is part of the longer cmdlet description.</para>
    /// <para type="description">Also part of the longer cmdlet description.</para>
    /// <para type="description">Asks the user a flavor, which the Cmdlet will store for later use. Currently this has no use and is only a test.</para>
    /// <para type="syntax">Get-Gizmo</para>
    /// <para type="inputType">System.String</para>
    /// <para type="inputTypeDescription">Takes the input and uses it as a string</para>
    /// <para type="linkTitle">Google</para>
    /// <para type="link">www.Google.com</para>
    /// <para type="linkTitle">Get-ValidationQuantity</para>
    /// <para type="linkTitle">Get-ValidationPosition</para>
    /// <para type="linkTitle">Send-Greeting</para>
    /// </summary>
    [Cmdlet(VerbsCommon.Get, "Gizmo")]
    public class GetGizmo : Cmdlet
    { 
        /// <summary>
        /// <para type="description">The flavor for the gizmo.</para>
        /// <para type="description">The test for the Gizmo</para>
        /// </summary>
        [Parameter(Position = 0,
               Mandatory = true,
               ValueFromPipeline = true,
               HelpMessage = "Enter the flavor name, e.g. Marshmallow")]
        [Alias("Aroma", "JeNeSaisQuoi")]
        public FlavorTypes Flavor { get; set; }

        /// <summary>
        /// <para type="description">The quantity for the gizmo.</para>
        /// </summary>
        [Parameter()]
        public int Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }
        private int _quantity = 42;


        protected override void ProcessRecord()
        {
            base.ProcessRecord();
        }
    }

    public enum FlavorTypes
    {
        Chocolate,
        Vanilla,
        Marshmallow
    }
}
