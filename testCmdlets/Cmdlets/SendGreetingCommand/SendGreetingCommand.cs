﻿using System.Management.Automation;  // Windows PowerShell assembly.

namespace CmdletTest.SendGreetingCommand
{
    // Declare the class as a cmdlet and specify the
    // appropriate verb and noun for the cmdlet name.
    /// <summary>
    /// <para type="synopsis">Sends a Greeting with the given name to the PS user.</para>
    /// <para type="descrition">Requires the user to write a name to the PS. This name will be used as a string and given back to the PS User with a small greeting.</para>
    /// <para type="syntax">Send-Greeting</para>
    /// <para type="inputType">System.String</para>
    /// <para type="inputTypeDescription">Takes the input and uses it as a string</para>
    /// <para type="returnValue">System.String</para>
    /// <para type="returnValueDescription">Greeting with the inputted name</para>
    /// <para type="returnValueDescription">The cmdlet will greet the User with the previously set name.</para>
    /// <para type="linkTitle">Google</para>
    /// <para type="link">www.Google.com</para>
    /// <para type="linkTitle">Get-ValidationQuantity</para>
    /// <para type="linkTitle">Get-ValidationPosition</para>
    /// <para type="linkTitle">Get-Test</para>
    /// </summary>
    [Cmdlet(VerbsCommunications.Send, "Greeting")]
    public class SendGreetingCommand : Cmdlet
    {
        // Declare the parameters for the cmdlet.
        ///<summary>
        /// <para type="description">There are Greetings</para>
        ///</summary>
        [Parameter(Mandatory = true)]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private string name;
        // Override the ProcessRecord method to process
        // the supplied user name and write out a
        // greeting to the user by calling the WriteObject
        // method.
        protected override void ProcessRecord()
        {
            WriteObject("Hello " + name + "!");
        }
    }
}