﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using Gizmo;

namespace Template
{
    /// <summary>
    /// <para type="synopsis">This is a template</para> 
    /// <para type="description">This is a template</para>
    /// <para type="description">This Template won't appear as a navigationLink for the other cmdlets because its just a template.</para>
    /// <para type="syntax">Get-Template</para>
    /// <para type="inputType">System.String</para>
    /// <para type="inputTypeDescription">This program uses a string.</para>
    /// <para type="inputTypeDescription">The String represents a filepath</para>
    /// <para type="returnValue">System.String</para>
    /// <para type="returnValueDescription">This cmdlet returns a String</para>
    /// <para type="returnValueDescription">The string is pasted into the powershell console</para>
    /// <para type="returnValueDescription">It is just a test</para>
    /// </summary>
    [Cmdlet(VerbsCommon.Get, "Template")]
    [OutputType(typeof(string))]

    public class GetTemplate : GetGizmo
    {

        /// <summary>
        /// <para type="description">This is a Template.</para>
        /// <para type="description">There are 2 parameters.</para>
        /// </summary>
        [Parameter(Mandatory = true)]
        public string FileName { get; set; }


        /// <summary>
        /// <para type="description">This is a parameter named Description it currently does nothing</para>
        /// </summary>
        [Parameter(Mandatory = false)]
        public string Description { get; set; }

        void Template()
        {
            Console.WriteLine("This is a template for the Get-Help Command and the including help xml.");
        }
        /// <summary>
        /// <para type="alertTitle">This is a Template.</para>
        /// <para type="alert">ALERT! This is a Template</para>
        /// <para type="alert">This is a second test alert</para>
        /// <para type="alertTitle">This is also a Template.</para>
        /// <para type="alert">ALERT! ALERT! This is a Template</para>
        /// <para type="alert">This is a second second test alert</para> 
        /// <para type="exampleTitle">----------  EXAMPLE TEMPLATE  ----------</para>
        /// <para type="example">Test paragraph Template</para>
        /// <para type="linkTitle">Get-Test</para>
        /// <para type="linkTitle">Get-ValidationQuantity</para>
        /// <para type="linkTitle">Google:</para>
        /// <para type="link">https://www.google.de</para>
        /// <para type="linkTitle">Get-ValidationPosition</para>
        /// <para type="linkTitle">Send-Greeting</para>
        /// <para type="linkTitle">Get-Gizmo</para>
        /// </summary>
        protected override void ProcessRecord()
        {
            Console.WriteLine(FileName);
            Template();
        }
    }
}

