﻿namespace Passwords
{
    public class Password
    {
        internal string PasswordString { get; set; } = "";
        public char KeyLetter { get; private set; }
        public int StartIndex { get; private set; }
        public int EndIndex { get; private set; }

        public Password(string passwordString, char keyLetter, int startIndex, int endIndex)
        {
            PasswordString = passwordString;
            KeyLetter = keyLetter;
            StartIndex = startIndex;
            EndIndex = endIndex;
        }

        public Password(string passwordString, string keyLetter, string startIndex, string endIndex)
            : this(passwordString, char.Parse(keyLetter), int.Parse(startIndex), int.Parse(endIndex))
        {

        }

        public bool IsValidQuantity()
        {
            var letterCount = 0;
            // check whether the password is Valid and if so change the isValid attribute to true
            foreach (var letter in PasswordString)
            {
                if (letter == KeyLetter)
                {
                    letterCount += 1;
                }
            }
            if (letterCount >= StartIndex && letterCount <= EndIndex)
            {
                return true;
            }
            return false;

        }

        public bool IsValidPosition()
        {

            // check whether the password is Valid and if so change the isValid attribute to true for the second task
            if (PasswordString[StartIndex - 1] == KeyLetter || PasswordString[EndIndex - 1] == KeyLetter)
            {
                if (PasswordString[StartIndex - 1] == PasswordString[EndIndex - 1])
                {
                    return false;
                }
                return true;
            }
            return false;
        }
    }
}