﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace GetXml2CmdletHelp.GenerateXml
{

    public class Commandlet
    {
        public string Verb { get; set; }

        public string Noun { get; set; }

        public string Name { get; set; }

        public CmdletXmlComments XmlComments { get; set; }

        public Commandlet(string filePath)
        {
            Name = GetName(filePath);
            Verb = GetVerb();
            Noun = GetNoun();
            XmlComments = new CmdletXmlComments(filePath);
        }

        //Get the full Cmdlet name
        public static string GetName(string filePath)
        {
            var name = "";
            foreach (var line in File.ReadAllLines(filePath))
            {
                if (line.Trim().StartsWith("[Cmdlet"))
                {
                    var trimmedLine = line.Trim('[', ']', ' ', '(', ')', '"', ')');
                    var splitline = trimmedLine.Split(',');
                    splitline[0] = Regex.Replace(splitline[0], "Cmdlet", "");

                    try
                    {
                        var preVerb = splitline[0].Trim(' ', '"', '(').Split('.');
                        var verb = preVerb[1];
                        var noun = splitline[1].Trim(' ', '"');
                        name = verb + "-" + noun;
                    }
                    catch (Exception)
                    {
                        var verb = splitline[0].Trim(' ', '"', '(');
                        var noun = splitline[1].Trim(' ', '"');
                        name = verb + "-" + noun;
                    }
                    break;
                }
            };
            return name;
        }

        //Get the Cmdlet Verb
        public string GetVerb()
        {
            var name = Name;
            var splitName = name.Split("-");
            return splitName[0];
        }

        //Get the cmdlet Noun
        public string GetNoun()
        {
            var name = Name;
            try
            {
                var names = name.Split("-");
                return names[1];
            }
            catch (Exception)
            {
                var names = name;
                return names;
            }
        }

        public class CmdletXmlComments
        {
            public string? Synopsis { get; set; }

            public List<string>? Description { get; set; } = new();

            public string? Syntax { get; set; }

            public List<Parameter> Parameters { get; set; } = new();

            public string? InputType { get; set; }

            public List<string>? InputTypeDescription { get; set; } = new();

            public string? ReturnValues { get; set; }

            public List<string>? ReturnValuesDescription { get; set; } = new();

            public List<string>? AlertTitle { get; set; } = new();

            public Dictionary<string,List<string>>? Alerts { get; set; } = new();

            public string? ExampleTitle { get; set; }

            public string? Examples { get; set; }

            public List<string>? LinkTitle { get; set; } = new();

            public Dictionary<string, string>? RelatedLinks { get; set; } = new();


            public CmdletXmlComments(string filePath)
            {
                var xmlComments = SeperateParameterAndNormalComments(filePath);
                SortComments(xmlComments);
            }

            public string SeperateParameterAndNormalComments(string filePath)
            {
                var isXmlComment = false;
                var xmlCommentsCache = "";
                var xmlComments = "";
                var fileLines = File.ReadAllLines(filePath);

                //This foreach loops sorts the xmlcomments in a codefile. It decides between normal comments and parameter comments.
                for (var i = 0; i < fileLines.Length; i++)
                {
                    var fileLine = fileLines[i].Trim();

                    if (fileLine.StartsWith("/// <summary>"))
                    {
                        isXmlComment = true;
                    }
                    else if (fileLine.StartsWith("/// </summary>"))
                    {
                        isXmlComment = false;
                    }
                    else if (isXmlComment)
                    {
                        xmlCommentsCache += fileLine + "\n";
                    }
                    //this part tries to fetch all Parameters from inherited Classes.
                    else if (fileLine.StartsWith("[Cmdlet"))
                    {
                        for (var j = i + 1; j < fileLines.Length; j++)
                        {
                            try
                            {
                                if (fileLines[j].Trim().StartsWith("[OutputType"))
                                {
                                    fileLines[j] = Regex.Replace(fileLines[j].Trim('[', ']', '(', ' '), "OutputType", "");
                                    fileLines[j] = fileLines[j].Trim('(');
                                }
                                else if (fileLines[j].Trim().Split(" ")[1] == "class")
                                {
                                    var className = fileLines[j].Trim().Split(" ")[4];
                                    if (className != "Cmdlet")
                                    {
                                        className = className + ".cs";
                                        var inheritedFiles = Directory.GetFiles(@"C:\DEV\GetXml2CmdletHelp", className, SearchOption.AllDirectories);
                                        foreach (var inheritedFile in inheritedFiles)
                                        {
                                            SeperateParameterAndNormalComments(inheritedFile);
                                        }
                                    }
                                    break;
                                }
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                    else if (fileLine.StartsWith("[Parameter"))
                    {
                        var parameter = new Parameter(xmlCommentsCache, i, File.ReadAllLines(filePath));
                        Parameters.Add(parameter);
                        xmlCommentsCache = "";
                    }
                    else if (!(xmlCommentsCache == ""))
                    {
                        xmlComments += xmlCommentsCache;
                        xmlCommentsCache = "";
                    }
                }
                return xmlComments;
            }

            //Sorts all xml Comments except of the ones connected to parameters
            public void SortComments(string xmlComments)
            {
                var testList = new List<string>();
                var filled = false;
                var alert = false;
                for (var i = 0; i < xmlComments.Split("\n").Length; i++)
                {
                    var xmlComment = xmlComments.Split("\n")[i];
                    var comment = xmlComment.Trim();
                    comment = Regex.Replace(comment, "/// <para type=\"", "");
                    var splitComments = comment.Split("\"");
                    try
                    {
                        splitComments[1] = string.Join("",
                               splitComments[1].SkipWhile(p => p != '>')
                                   .Skip(1)
                                   .TakeWhile(p => p != '<')
                              );
                        if (splitComments[0].Equals("synopsis"))
                        {
                            Synopsis = splitComments[1];
                        }
                        else if (splitComments[0].Equals("description"))
                        {
                            Description?.Add(splitComments[1]);
                        }
                        else if (splitComments[0].Equals("syntax"))
                        {
                            Syntax = splitComments[1];
                        }
                        else if (splitComments[0].Equals("inputType"))
                        {
                            InputType = splitComments[1];
                        }
                        else if (splitComments[0].Equals("inputTypeDescription"))
                        {
                            InputTypeDescription.Add(splitComments[1]);
                        }
                        else if (splitComments[0].Equals("returnValue"))
                        {
                            ReturnValues = splitComments[1];
                        }
                        else if (splitComments[0].Equals("returnValueDescription"))
                        {
                            ReturnValuesDescription.Add(splitComments[1]);
                        }
                        else if (splitComments[0].Equals("exampleTitle"))
                        {
                            ExampleTitle = splitComments[1];
                        }
                        else if (splitComments[0].Equals("example"))
                        {
                            Examples = splitComments[1];
                        }
                        else if (splitComments[0].Equals("linkTitle"))
                        {
                            LinkTitle?.Add(splitComments[1]);
                        }
                        else if (splitComments[0].Equals("link"))
                        {
                            if (LinkTitle != null)
                            {
                                string lastLinkTitleEntry = LinkTitle[^1];
                                RelatedLinks.Add(lastLinkTitleEntry, splitComments[1]);
                            }
                        }
                        //this seemingly random if statement needs to be here, because else the last alert dictionary would always be skipped.
                        if (splitComments[0].Equals("alertTitle") || alert == true)
                        {
                            if (splitComments[0] == "alert")
                            {
                                testList.Add(splitComments[1]);
                                continue;
                            }
                            else if (filled == true)
                            {
                                Alerts.Add(AlertTitle[^1], testList);
                                AlertTitle.Add(splitComments[1]);
                                testList = new List<string>();
                                alert = false;
                            }
                            else
                            {
                                AlertTitle.Add(splitComments[1]);
                                filled = true;
                            }
                        }
                        else if (splitComments[0].Equals("alert"))
                        {
                            testList.Add(splitComments[1]);
                            alert = true;
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }


        public class Parameter
        {
            public string? ParameterName { get; set; }

            public string? ParameterReturnValue { get; set; }

            public ParameterAttributes? Attributes { get; set; }

            public List<string>? ParameterDescriptions { get; set; } = new();

            public Parameter(string xmlCommentsCache,int enumerator,string[] fileLines)
            {
                var parameterNameLine = false;
                var parameterAttributes = "";

                for (var i = enumerator; i < fileLines.Length; i++)
                {
                    if (fileLines[i].Trim().StartsWith("[Parameter"))
                    {
                        if (fileLines[i].EndsWith("]"))
                        {
                            parameterAttributes += fileLines[i];
                            parameterNameLine = true;
                            break;
                        }
                    }
                    else if (fileLines[i].EndsWith("]"))
                    {
                        parameterAttributes += fileLines[i];
                        parameterNameLine = true;
                        enumerator = i;
                        break;
                    }
                    else
                    {
                        parameterAttributes += fileLines[i];
                    }
                }
                if (parameterNameLine)
                {
                    var AliasLine = "";
                    enumerator++;
                    if (fileLines[enumerator] != null || fileLines[enumerator] != "")
                    {
                        if (fileLines[enumerator].Trim().StartsWith("[Alias"))
                        {
                            AliasLine = fileLines[enumerator].Trim();
                            enumerator++;
                        }
                        if (fileLines[enumerator].Trim().StartsWith("[ValidateSet"))
                        {
                            enumerator++;
                        }
                        if (fileLines[enumerator].Trim().StartsWith("[Credential"))
                        {
                            enumerator++;
                        }
                        var parameterNameList = fileLines[enumerator].Trim().Split(" ");
                        var parameterReturnValue = parameterNameList[1];
                        var parameterName = parameterNameList[2];
                        parameterNameLine = false;

                        foreach (var description in xmlCommentsCache.Trim().Split("\n"))
                        {
                            var xmlComment = description.Trim();
                            xmlComment = Regex.Replace(xmlComment, "/// <para type=\"", "");
                            xmlComment = string.Join("",
                                    xmlComment.SkipWhile(p => p != '>')
                                        .Skip(1)
                                        .TakeWhile(p => p != '<')
                                    );
                            ParameterDescriptions.Add(xmlComment);
                        }
                        var attributes = new ParameterAttributes(parameterAttributes, AliasLine);

                        Attributes = attributes;
                        ParameterName = parameterName;
                        ParameterReturnValue = parameterReturnValue;
                    }
                }
                       
            }


            public class ParameterAttributes
            {
                public bool Required { get; set; } = false;

                //public bool VariableLength { get; set; } = false;

                public bool Globbing { get; set; } = false;

                //public bool PipelineInput { get; set; } = false;

                //public string? Position { get; set; } = "named";

                public List<string>? Aliases { get; set; } = new List<string>();

                public string? HelpMessage = "";

                public ParameterAttributes(string xmlCommentsCache, string aliasLine)
                {
                    xmlCommentsCache = Regex.Replace(xmlCommentsCache.Trim('[', ']', '(', ')', ' '), "Parameter", "");
                    xmlCommentsCache = xmlCommentsCache.Trim('(');
                    foreach (var xmlComment in xmlCommentsCache.Split(","))
                    {
                        try
                        {
                            var splitted = xmlComment.Trim(' ', '(').Split('=');
                            if (splitted[0].Trim() == "Mandatory")
                            {
                                if (splitted[1].Trim() == "true")
                                {
                                    Required = true;
                                }
                                else
                                {
                                    Required = false;
                                }
                            }
                            else if (splitted[0].Trim() == "Globbing")
                            {
                                if (splitted[1].Trim() == "true")
                                {
                                    Globbing = true;
                                }
                                else
                                {
                                    Globbing = false;
                                }
                            }
                            else if (splitted[0].Trim() == "HelpMessage")
                            {
                                HelpMessage = splitted[1].Trim(' ', '"', '@');
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                    aliasLine = aliasLine.Trim('[', ']');
                    aliasLine = Regex.Replace(aliasLine, "Alias", "");
                    aliasLine = Regex.Replace(aliasLine, "\"", "");
                    aliasLine = aliasLine.Trim('(', ')');
                    if (aliasLine == "")
                    {
                        Aliases.Add("none");
                    }
                    else
                    {
                        foreach (var alias in aliasLine.Split(','))
                        {
                            Aliases.Add(alias.Trim());
                        }
                    }
                }
            }
        }
    }
}