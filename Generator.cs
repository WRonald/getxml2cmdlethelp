﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Management.Automation;
using System.Reflection;

namespace GetXml2CmdletHelp.GenerateXml
{
    public class Generator
    {
        private static readonly XNamespace MshNs = XNamespace.Get("http://msh");
        private static readonly XNamespace MamlNs = XNamespace.Get("http://schemas.microsoft.com/maml/2004/10");
        private static readonly XNamespace CommandNs = XNamespace.Get("http://schemas.microsoft.com/maml/dev/command/2004/10");
        private static readonly XNamespace DevNs = XNamespace.Get("http://schemas.microsoft.com/maml/dev/2004/10");
        private static readonly XNamespace MSHelp = XNamespace.Get("http://msdn.microsoft.com/mshelp");



        public XDocument Generate(string xmlFilePath, string[] codeFiles)
        {
            var document = new XDocument(new XDeclaration("1.0", "utf-8", null),
                                        GenerateHelpItems(codeFiles));
            document.Save(xmlFilePath);
            System.Diagnostics.Process.Start(@"C:\Program Files\Notepad++\notepad++.exe", xmlFilePath);
            return document;
        }


        private static XElement GenerateHelpItems(string[] codeFiles)
        {
            var helpItems = new XElement(MshNs + "helpItems", new XAttribute("schema", "maml"));
            foreach (var codeFile in codeFiles)
            {
                foreach (var line in File.ReadAllLines(codeFile))
                {
                    if (line.Trim().StartsWith("[Cmdlet"))
                    {
                        var commandlet = new Commandlet(codeFile);
                        helpItems.Add(GenerateCommand(commandlet));
                        break;
                    }
                }   
            }
            return helpItems;
        }

        private static XElement GenerateCommand(Commandlet commandlet)
        {
            return new XElement(CommandNs + "command",
                    new XAttribute(XNamespace.Xmlns + "maml", MamlNs),
                    new XAttribute(XNamespace.Xmlns + "command", CommandNs),
                    new XAttribute(XNamespace.Xmlns + "dev", DevNs),
                    new XAttribute(XNamespace.Xmlns + "MSHelp", MSHelp),
                      GenerateDetails(commandlet),
                      GenerateDescription(commandlet),
                      GenerateSyntax(commandlet),
                      GenerateParameters(commandlet),
                      GenerateInputTypes(commandlet),
                      GenerateReturnValues(commandlet),
                      GenerateAlert(commandlet),
                      GenerateExamples(commandlet),
                      GenerateRelatedLinks(commandlet));
        }

        private static XElement GenerateDetails(Commandlet commandlet)
        {
            return new XElement(CommandNs + "details",
                                new XElement(CommandNs + "name", commandlet.Name),
                                new XElement(CommandNs + "verb", commandlet.Verb),
                                new XElement(CommandNs + "noun", commandlet.Noun),
                                new XElement(MamlNs + "description", 
                                    new XElement( MamlNs + "para", commandlet.XmlComments.Synopsis)));
                        
        }

        private static XElement GenerateDescription(Commandlet commandlet)
        {
            var description = new XElement(MamlNs + "description");
            if (commandlet.XmlComments.Description != null)
            {
                foreach (var cmdlet in commandlet.XmlComments.Description)
                {
                    description.Add(new XElement(MamlNs + "para", cmdlet));
                }
            }
            return description;
        }

        private static XElement GenerateSyntax(Commandlet commandlet)
        {
            var syntaxElement = new XElement(CommandNs + "syntax",
                                    new XElement(CommandNs + "syntaxItem",
                                        new XElement(MamlNs + "name"),
                                        new XElement(CommandNs + "parameter",
                                            new XElement(MamlNs + "name"),
                                            new XElement(MamlNs + "description",
                                                new XElement(MamlNs + "para", commandlet.XmlComments.Syntax)),
                                            new XElement(CommandNs + "parameterValue"),
                                            new XElement(DevNs + "type",
                                                new XElement(MamlNs + "name"),
                                                new XElement(MamlNs + "uri")),
                                            new XElement(DevNs + "defaultValue"))));
            return syntaxElement;
        }




        private static XElement GenerateParameters(Commandlet commandlet)
        {
            var parametersElement = new XElement(CommandNs + "parameters");
            foreach (var parameter in commandlet.XmlComments.Parameters)
            {
                try
                {
                    parametersElement.Add(new XElement(MamlNs + "parameter",
                                            new XAttribute("required", parameter.Attributes.Required),
                                            new XAttribute("variableLength", false),
                                            new XAttribute("globbing", parameter.Attributes.Globbing),
                                            new XAttribute("pipeLineInput", false),
                                            new XAttribute("position", 1),
                                            new XAttribute("aliases", GenerateParameterAlias(parameter.Attributes.Aliases)),
                                              new XElement(MamlNs + "name", parameter.ParameterName),
                                              new XElement(GenerateParametersDescription(parameter)),
                                              new XElement(CommandNs + "parameterValue", "test",
                                                new XAttribute("required", false),
                                                new XAttribute("variableLength", false)),
                                              new XElement(DevNs + "type",
                                                  new XElement(MamlNs + "name"),
                                                  new XElement(MamlNs + "uri")),
                                              new XElement(DevNs + "defaultValue")));
                }
                catch (NullReferenceException)
                {
                }
            }
            return parametersElement;
        }

        private static XElement GenerateParametersDescription(Commandlet.Parameter parameter)
        {
            var descriptionElement = new XElement(MamlNs + "description");

            if (parameter.Attributes.HelpMessage != "" || parameter.Attributes.HelpMessage != null)
            {
                descriptionElement.Add(new XElement(MamlNs + "para", parameter.Attributes.HelpMessage));
            }
            foreach (var description in parameter.ParameterDescriptions)
            {
                descriptionElement.Add(new XElement(MamlNs + "para", description));
            }

            return descriptionElement;
        }

        private static string GenerateParameterAlias(List<string> aliases)
        {
            var aliasesString = "";
            foreach(var alias in aliases)
            {
                aliasesString += alias + ", ";
            }
            return aliasesString.Trim(' ',',');
        }

        private static XElement GenerateInputTypes(Commandlet commandlet)
        {
            var inputTypesElement = new XElement(CommandNs + "inputTypes",
                                        new XElement(CommandNs + "inputType",
                                            new XElement(DevNs + "type",
                                                new XElement(MamlNs + "name", commandlet.XmlComments.InputType)),
                                            new XElement(GenerateInputTypesDescriptions(commandlet))));
            return inputTypesElement;
        }

        private static XElement GenerateInputTypesDescriptions(Commandlet commandlet)
        {
            var inputTypeDescription = new XElement(MamlNs + "description");

            if (commandlet.XmlComments.InputTypeDescription != null)
            {
                foreach (var description in commandlet.XmlComments.InputTypeDescription)
                {
                    inputTypeDescription.Add(new XElement(MamlNs + "para", description));
                }
            }
            return inputTypeDescription;
        }

        private static XElement GenerateReturnValues(Commandlet commandlet)
        {
            var returnValueElement = new XElement(CommandNs + "returnValues",
                            new XElement(CommandNs + "returnValue",
                                new XElement(DevNs + "type",
                                    new XElement(MamlNs + "name", commandlet.XmlComments.ReturnValues)),
                                new XElement(GenerateReturnValuesDescriptions(commandlet))));

            return returnValueElement;
        }

        private static XElement GenerateReturnValuesDescriptions(Commandlet commandlet)
        {
            var returnValueDescription = new XElement(MamlNs + "description");

            if (commandlet.XmlComments.ReturnValuesDescription != null)
            {
                foreach (var description in commandlet.XmlComments.ReturnValuesDescription)
                {
                    returnValueDescription.Add(new XElement(MamlNs + "para", description));
                }
            }
            return returnValueDescription;
        }


        private static XElement GenerateAlert(Commandlet commandlet)
        {
            var AlertSet = new XElement(MamlNs + "AlertSet");

            if (commandlet.XmlComments.Alerts != null)
            {
                foreach (var alertKey in commandlet.XmlComments.Alerts.Keys)
                {
                    AlertSet.Add(new XElement(MamlNs + "title",alertKey),
                                    GenerateAlertPara(commandlet, alertKey));                                              
                }
            }
            return AlertSet;
        }

        private static XElement GenerateAlertPara(Commandlet commandlet, string keyword)
        {
            var alertPara = new XElement(MamlNs + "alert");

            foreach (var alert in commandlet.XmlComments.Alerts[keyword])
            {
                alertPara.Add(new XElement(MamlNs + "para", alert));
            }
            return alertPara;
        }

        private static XElement GenerateExamples(Commandlet commandlet)
        {
            var GenerateExamples = new XElement(CommandNs + "Examples",
                                        new XElement(CommandNs + "example",
                                            new XElement(MamlNs + "title", commandlet.XmlComments.ExampleTitle),
                                        new XElement(MamlNs + "introduction",
                                            new XElement(MamlNs + "paragraph", commandlet.XmlComments.Examples))));
            return GenerateExamples;
        }


        private static XElement GenerateRelatedLinks(Commandlet commandlet)
        {
            var RelatedLinks = new XElement(MamlNs + "RelatedLinks");
            if (commandlet.XmlComments.LinkTitle != null)
            {
                foreach (var cmdletXmlComment in commandlet.XmlComments.LinkTitle)
                {
                    RelatedLinks.Add(new XElement(MamlNs + "navigationLink",
                                        new XElement(MamlNs + "linkText", cmdletXmlComment),
                                        GenerateWebLink(commandlet,cmdletXmlComment)));
                }
            }
            return RelatedLinks;    
        }

        private static XElement GenerateWebLink(Commandlet commandlet, string keyWord)
        {
            var webLink = new XElement(MamlNs + "uri");

            if (commandlet.XmlComments.RelatedLinks != null)
            {
                if (commandlet.XmlComments.RelatedLinks.ContainsKey(keyWord))
                {
                    webLink.Add(commandlet.XmlComments.RelatedLinks[keyWord]);
                }
            }
            return webLink;
        }
    }
}