﻿using GetXml2CmdletHelp.GenerateXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.IO;

namespace GetXml2CmdletHelp 
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string[] files = Directory.GetFiles(@"C:\DEV\GetXml2CmdletHelp\testCmdlets\Cmdlets", "*.cs", SearchOption.AllDirectories);
            var generator = new Generator();
            generator.Generate(@"C:\Windows\System32\WindowsPowerShell\v1.0\Modules\TestCmdlet2\en-US\TestCmdlet2.dll-help.xml", files);
        }

    }

}

